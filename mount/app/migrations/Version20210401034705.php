<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210401034705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Change analysis_result.regexp_dataset_value on nullable';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE analysis_result CHANGE regexp_dataset_value regexp_dataset_value VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE analysis_result CHANGE regexp_dataset_value regexp_dataset_value VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
