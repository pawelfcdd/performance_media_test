<?php


namespace App\Service;

use App\Service\HuaweiPushProviderService\HuaweiPushProvider;
use App\Service\HuaweiPushProviderService\Structure\Message;
use App\Service\HuaweiPushProviderService\Structure\Notification;
use App\Service\HuaweiPushProviderService\Structure\NotificationPayload;

class HuaweiPushProviderService
{
    private HuaweiPushProvider $huaweiPushProvider;

    public function __construct(HuaweiPushProvider $huaweiPushProvider)
    {
        $this->huaweiPushProvider = $huaweiPushProvider;
    }

    public function sendPush(array $data)
    {
        $response = $this->huaweiPushProvider->getAccessToken()->push(
            NotificationPayload::make()
                ->setValidateOnly(false)
                ->setMessage(
                    Message::make()
                        ->setNotification(
                            Notification::make()
                                ->setTitle('title')
                                ->setBody('body')
                                ->setImage('image')
                        )
                )
        );

        dd($response);
        return 1;
    }
}