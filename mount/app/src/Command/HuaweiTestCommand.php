<?php


namespace App\Command;


use App\Service\HuaweiPushProviderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HuaweiTestCommand extends Command
{
    private HuaweiPushProviderService $huawei;

    public function __construct(HuaweiPushProviderService $huawei)
    {
        parent::__construct();
        $this->huawei = $huawei;
    }

    public function configure()
    {
        $this->setName('huawei:send-push');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $data = [
            [
                'title' => 'Message 1',
                'body' => 'Body 1'
            ],
            [
                'title' => 'Message 2',
                'body' => 'Body 2'
            ],
            [
                'title' => 'Message 3',
                'body' => 'Body 3'
            ],
        ];

        $response = $this->huawei->sendPush($data);

        dd($response);

        return 0;
    }
}