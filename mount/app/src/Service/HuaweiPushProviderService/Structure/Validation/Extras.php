<?php


namespace App\Service\HuaweiPushProviderService\Structure\Validation;


interface Extras
{
    /**
     * @return mixed
     */
    public function validate();

    /**
     * @return array
     */
    public function toArray();
}