## Steps to install

1. Run `docker-compose up -d --build` to start building containers
2. Run `docker-compose exec app bash` to login into app container
3. Inside the app container run `composer install`
4. Inside the app container type `cd public/ && npm install`
5. Inside the app container run `bin/console doctrine:database:create && bin/console doctrine:migrations:migrate` 
   to create the DB schema with tables
   
## Run application

1. To run this application go to `http://localhost` in Your browser