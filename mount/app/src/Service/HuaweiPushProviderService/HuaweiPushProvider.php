<?php

namespace App\Service\HuaweiPushProviderService;


use Bernard\Producer;
use App\Service\AbstractPushProvider;
use App\Service\HuaweiPushProviderService\Client\Http;
use App\Service\HuaweiPushProviderService\Constant\Endpoint;
use App\Service\HuaweiPushProviderService\Exception\HuaweiException;
use App\Service\HuaweiPushProviderService\Structure\NotificationPayload;

class HuaweiPushProvider
{
    public const APP_ID = '104056305';
    public const CLIENT_SECRET = '6a209b0579d94d07e6535aadb6cc6a419b4410d0233426da3529c1e53a4ddc75';

    protected array $config = [];
//
//    public function __construct(array $config = [])
//    {
//        parent::__construct();
//        $this->config = $config;
//
//        if (!($config['app_id'] ?? null)) {
//            throw new HuaweiException("Missing Client ID");
//        }
//
//        if (!($config['client_secret'] ?? null)) {
//            throw new HuaweiException("Missing Client Secret");
//        }
//    }


    public function getAccessToken()
    {
        $response = Http::asForm()
            ->post(Endpoint::TOKEN, [
                'grant_type' => 'client_credentials',
                'client_id' => self::APP_ID,
                'client_secret' => self::CLIENT_SECRET,
            ]);

        if (!$response->ok()) {
            throw new HuaweiException($response->json()['error_description'] ?? 'Unknown Error', $response->status());
        }

        $response = $response->json();
        $this->config['access_token'] = $response['access_token'];

        return $this;
    }

    public function withAccessToken($accessToken)
    {
        $this->config['access_token'] = $accessToken;
        return $this;
    }

    public function push(NotificationPayload $payload)
    {
        $response = Http::asJson()
            ->withHeaders([
                'Authorization' => "Bearer {$this->config['access_token']}"
            ])
            ->post(
                str_replace('{appid}', self::APP_ID, Endpoint::PUSH),
                $payload->toArray()
            );

        return $response->json();
    }

    public function processPushJob(array $data)
    {
        // TODO: Implement processPushJob() method.
    }

    /**
     * @param array|null $data
     * @return $this
     */
    public function make(?array $data): self
    {
        return $this;
    }
}