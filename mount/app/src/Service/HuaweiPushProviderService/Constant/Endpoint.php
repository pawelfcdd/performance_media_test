<?php


namespace App\Service\HuaweiPushProviderService\Constant;


class Endpoint
{
    public const PUSH = 'https://push-api.cloud.huawei.com/v1/{appid}/messages:send';
    public const TOKEN = 'https://oauth-login.cloud.huawei.com/oauth2/v3/token';
}