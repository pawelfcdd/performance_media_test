<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210401030825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create analysis_result table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE analysis_result (id INT AUTO_INCREMENT NOT NULL, api_dataset_value INT NOT NULL, regexp_dataset_value VARCHAR(255) NOT NULL, comparison_result INT NOT NULL, rule_checker_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE analysis_result');
    }
}
