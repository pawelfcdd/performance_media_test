<?php


namespace App\Service;


use Bernard\Message\PlainMessage;
use Bernard\Producer;

abstract class AbstractPushProvider
{
    /**
     * Push messages count per one request
     */
    const MESSAGES_LIMIT = 1;

    /**
     * @var Producer
     */
    protected $producer;

    /**
     * @var array
     */
    protected $stack = [];

    public function __construct(Producer $producer) {
        $this->producer = $producer;
    }

    public function getProviderName() {
        return (strtolower(str_replace('PushProvider', '', class_basename(static::class))));
    }

    public function add(array $data) {
        $this->stack[] = $data;
    }

    public function queuePushJobs() {
        if (!empty($this->stack)) {
            $stack = $this->stack;
            $this->stack = [];

            foreach (array_chunk($stack, static::MESSAGES_LIMIT) as $chunk) {
                $this->queuePushJob($chunk);
            };
        }
    }

    protected function queuePushJob(array $chunk) {
        $this->producer->produce(new PlainMessage('sendPushes', ['type' => $this->getProviderName(), 'data' => $chunk]), 'pushes');
    }

    public abstract function processPushJob(array $data);
}